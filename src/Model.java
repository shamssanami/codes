
public class Model {

	public int grid[][];
		
	public Model(int row, int column){
		grid = new int[row][column];
	}

	public void printGridText(){
		
		for(int row=0;row!=grid.length-1; row++){
			for(int column =0; column!=grid.length -1; column++){
				System.out.print(" + " + grid[row][column]);
			}
			System.out.println();
		}
	}
}
