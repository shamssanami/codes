import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.Icon;


public class MyShape implements Icon{

	private int width;
	private int height;
	private Color color;
	
	public MyShape(int width, int height, Color c){
		this.width = width;
		this.height = height;
		this.color = c;
	}
	
	@Override
	public void paintIcon(Component c, Graphics g, int x, int y) {
		Graphics2D g2 = (Graphics2D) g;
		Rectangle2D.Double rectangle = new Rectangle2D.Double(0, 0, x, y);
	
		g2.fill(rectangle);
		g2.setPaint(color);
		g2.setColor(color);
		g2.draw(rectangle);
		
	}

	@Override
	public int getIconWidth() {
		// TODO Auto-generated method stub
		return this.width;
	}

	@Override
	public int getIconHeight() {
		// TODO Auto-generated method stub
		return this.height;
	}
	
	public void setColor(Color c){
		this.color =c;
	}
}
